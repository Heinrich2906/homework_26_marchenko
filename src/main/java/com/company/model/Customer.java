package com.company.model;

import java.sql.Timestamp;

/**
 * Created by heinr on 27.11.2016.
 */
public class Customer {

    private Long id;
    private String firstName;
    private String lastName;
    private Timestamp birthDay;
    private String address;
    private String city;
    private String passport;
    private String phone;

    public Customer(Long id, String firstName, String lastName, Timestamp birthDay, String address, String city, String passport, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.address = address;
        this.city = city;
        this.passport = passport;
        this.phone = phone;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Timestamp getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Timestamp birthDay) {
        this.birthDay = birthDay;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString () {
        return "Name " + this.firstName + " Surname " + this.lastName + " birthday " + this.birthDay + " address + " + this.address + " city " + this.city + " passport " + this.passport + " phone " + this.phone;
    }
}
