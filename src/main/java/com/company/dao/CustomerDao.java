package com.company.dao;

import com.company.model.Customer;
import java.util.List;

/**
 * Created by heinr on 30.11.2016.
 */
public interface CustomerDao {

    Customer get(Long id) throws Exception;
    Long save(Customer customer);
    void update(Customer customer);
    void delete(Long customerId) throws Exception;
    List<Customer> list();
}
