package com.company.controller;

import com.company.service.CustomersService;
import com.company.model.Customer;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.text.DateFormat;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Timestamp;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by heinr on 25.12.2016.
 */
public class CreateCustomerServlet extends HttpServlet {

    private CustomersService customerService = new CustomersService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/createCustomer")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }

        // Генерируем ответ и записываем его в response
        // Подробнее см. IndexServlet
        resp.setCharacterEncoding("utf-8");

        //req.setAttribute("customers", customerService.list());

        Boolean answerNo = new Boolean(false);
        Boolean answerYes = new Boolean(true);

        req.setAttribute("isTransactionActive", answerNo);
        req.setAttribute("isCustomersActive", answerYes);
        req.setAttribute("isAccountActive", answerNo);
        req.setAttribute("showHouse", answerYes);

        req.getRequestDispatcher("/WEB-INF/jsp/createNewCustomer.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "create":

                String stringTime = params.get("birthdate")[0];

                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                try {
                    date = (Date) formatter.parse(stringTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Timestamp timeStampDate = new Timestamp(date.getTime());

                customerService.create(
                        // Достаём значения полей формы из параметров запроса
                        params.get("firstname")[0], // Альтернатива req.getParameter("firstname")
                        params.get("lastname")[0],
                        timeStampDate,
                        params.get("address")[0],
                        params.get("city")[0],
                        params.get("passport")[0],
                        params.get("phone")[0]
                );
                break;

        }

        resp.sendRedirect("/customers?id=-1");
    }
}
