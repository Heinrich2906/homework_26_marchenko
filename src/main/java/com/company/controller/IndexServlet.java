package com.company.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

// Класс сервлета наследуется от javax.servlet.http.HttpServlet
public class IndexServlet extends HttpServlet {

    // doGet обрабатывает GET запросы на http://localhost:8080/
    // см. файл web.xml
    // req - объект-запрос браузера
    // resp - объект ответ сервера
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        resp.setCharacterEncoding("utf-8");

        Boolean answerNo = new Boolean(false);

        req.setAttribute("isTransactionActive", answerNo);
        req.setAttribute("isCustomersActive", answerNo);
        req.setAttribute("isAccountActive", answerNo);
        req.setAttribute("showHouse", answerNo);

        req.getRequestDispatcher("/WEB-INF/jsp/homePage.jsp").forward(req, resp);
    }
}
