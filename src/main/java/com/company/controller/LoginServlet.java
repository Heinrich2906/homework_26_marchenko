package com.company.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by heinr on 25.12.2016.
 */
public class LoginServlet extends HttpServlet {

    private static final String LOGIN = "eric";
    private static final String PASSWORD = "cartman";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setCharacterEncoding("utf-8");

        req.getRequestDispatcher("/WEB-INF/jsp/loginPage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Получаем параметры из формы
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (LOGIN.equals(login) && PASSWORD.equals(password)) {

            // Получем сессию.
            // Если параметр true, то сессия будет создана, если её нет.
            // Если параметр false, и сессии нет, то метод вернет null.
            // Если сессия уже есть, метод getSession вернет ее независимо от параметра.
            HttpSession session = req.getSession(true);

            // Добавление атрибута к сессии
            session.setAttribute("login", LOGIN);
            session.setAttribute("username", "Eric Cartman");

            // Удаление атрибута
            // session.removeAttribute("blablabla");

            // Успешно авторизовались, перенаправляем на страницу кастомеров
            resp.sendRedirect("/index");
        } else {
            // Авторизация не удалась. Возвращаем на страницу логина
            resp.sendRedirect("/login");
        }
    }
}
