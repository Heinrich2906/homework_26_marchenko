package com.company.controller;

import com.company.model.Customer;
import com.company.service.CustomersService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public class CustomersServlet extends HttpServlet {

    private CustomersService customerService = new CustomersService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/customers")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }

        resp.setCharacterEncoding("utf-8");

        req.setAttribute("customers", customerService.list());

        Boolean answerNo = new Boolean(false);
        Boolean answerYes = new Boolean(true);

        req.setAttribute("isTransactionActive", answerNo);
        req.setAttribute("isCustomersActive", answerYes);
        req.setAttribute("isAccountActive", answerNo);
        req.setAttribute("showHouse", answerYes);

        req.getRequestDispatcher("/WEB-INF/jsp/customers.jsp").forward(req, resp);
    }

    // Обработчик POST запросов на
    // http://localhost:8080/customers/create
    // и
    // http://localhost:8080/customers/delete
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "delete":
                // Достаём поле id из формы удаления кастомера и удаляем соответствующую запись через сервис
                try {
                    Long customerId = Long.parseLong(params.get("id")[0]);
                    customerService.delete(customerId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

        resp.sendRedirect("/customers?id=-1");
    }

    // Метод - генератор HTML кода страницы кустомеров
    private String generateCustomerHtml(List<Customer> customers) {
        StringBuilder sb = new StringBuilder();
        for (Customer customer : customers)
            sb.append(createCustomerRow(customer));
        String userRows = sb.toString();

        return "<!DOCTYPE html> " +
                "<html> " +
                "<head> " +
                " <meta charset=\"utf-8\"> " +
                " <title>Krähenbank</title> " +
                " <link rel=\"stylesheet\" href=\"/css/style.css\"> " +
                "</head> " +
                "<body> " +
                    "<div class=\"container\">" +
                        " <img id=\"logo\" src=\"/img/Krauhe_Klar.png\"/> " +
                        " <h1 id=\"Krahe\">Krähenbank</h1>" +
                        "</div>" +
                    " <ul> " +
                "  <li><a class=\"active\">Kunden</a></li> " +
                "  <li><a href=\"/accounts?id=-1\">Konten</a></li> " +
                "  <li><a href=\"/transactions?id=-1\">Transaktionen</a></li> " +
                "  <ul class=\"home\"> " +
                "   <li><a href=\"/index\"><img src=\"../img/home.png\"></a></li> " +
                "  </ul> " +
                " </ul> " +
                " <div class=\"container\"> " +
                "  <div class=\"header\"> " +
                "Kunden" +
                "  </div> " +
                "  <div> " +
                "   <a href=\"/createCustomer\" class=\"btn\">Erstellen Sie Kunde</a> " +
                "  </div> " +
                "  <div class=\"table\"> " +
                "   <table> " +
                "    <thead> " +
                "     <tr> " +
                "      <th>Vorname</th> " +
                "      <th>Nachname</th> " +
                "      <th>Geburtstag</th> " +
                "      <th>Adresse</th> " +
                "      <th>Stadt</th> " +
                "      <th>Ausweis</th> " +
                "      <th>Phone</th> " +
                "      <th></th> " +
                "     </tr> " +
                "    </thead> " +
                "    <tbody> " +

                userRows +

                "    </tbody> " +
                "   </table> " +
                "  </div> " +
                " </div> " +
                "<script src=\"/javascript/script.js\"></script>" +
                "</body> " +
                "</html>";
    }

    // Метод - генератор HTML кода строки (элемент tr) для таблицы кастомеров
    private String createCustomerRow(Customer customer) {
        return "     <tr> " +
                "      <td><a href=\"http://localhost:8080/accounts?id=" + customer.getId().toString() + "\">" + customer.getFirstName() + "</a></td> " +
                "      <td>" + customer.getLastName() + "</td> " +
                "      <td>" + customer.getBirthDay() + "</td> " +
                "      <td>" + customer.getAddress() + "</td> " +
                "      <td>" + customer.getCity() + "</td> " +
                "      <td>" + customer.getPassport() + "</td> " +
                "      <td>" + customer.getPhone() + "</td> " +
                "      <td>" +
                "<form action=\"customers/delete\" method=\"post\">" +
                "<input type=\"hidden\" name=\"id\" value=\"" + customer.getId() + "\"/>" +
                "<button type=\"submit\" class=\"customer-delete\"></button>" +
                "</form>" +
                "      </td>" +
                "     </tr> ";
    }

}
