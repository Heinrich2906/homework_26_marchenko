package com.company.dto;

import com.company.model.Transaction;
import com.company.model.Account;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by heinr on 30.11.2016.
 */
public class AccountDto {

    private Long accountNumber;
    private BigDecimal balance;
    private Timestamp creationDate;
    private String currency;
    private boolean blocked;
    private Long customerId;

    private List<Transaction> transactions;

    AccountDto(Account account, List<Transaction> transactions) {

        this.accountNumber = account.getAccountNumber();
        this.balance = account.getBalance();
        this.creationDate = account.getCreationDate();
        this.currency = account.getCurrency();
        this.blocked = account.isBlocked();
        this.customerId = account.getCustomerId();

        this.transactions = new ArrayList<>();

        for (Transaction s : transactions) {
            this.transactions.add(s);
        }
    }
}