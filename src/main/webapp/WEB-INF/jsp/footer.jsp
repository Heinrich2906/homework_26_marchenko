<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.time.LocalDateTime" %>

<ul>

    <c:if test="${isCustomersActive}">
        <li><a class="active" href="/customers?id=-1">Customers</a></li>
    </c:if>
    <c:if test="${!isCustomersActive}">
        <li><a href="/customers?id=-1">Customers</a></li>
    </c:if>

    <c:if test="${isAccountActiv}">
        <li><a class="active" href="/accounts?id=-1">Accounts</a></li>
    </c:if>
    <c:if test="${!isAccountActiv}">
        <li><a href="/accounts?id=-1">Accounts</a></li>
    </c:if>

    <c:if test="${isTransactionActive}">
        <li><a class="active" href="/transactions?id=-1">Transactions</a></li>
    </c:if>
    <c:if test="${!isTransactionActive}">
        <li><a href="/transactions?id=-1">Transactions</a></li>
    </c:if>

    <c:if test="${showHouse}">
        <ul  class="home">
            <li><a href="/index"><img src="../img/home.png"></a></li>
        </ul>
    </c:if>

</ul>
