<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.company.model.Transaction" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <jsp:include page="footer.jsp"/>

        <div class="header">
            Transakionen
        </div>

        <div class="table">

            <table>

                <thead>
                    <tr>
                        <th>Accountnumber</th>
                        <th>Vorgangs-ID</th>
                        <th>Buchungstransaktionen</th>
                        <th>Datum</th>
                        <th>Balance</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                <c:forEach items="${requestScope.transactions}" var="transaction">
                    <tr>
                        <td>${transaction.accountNumber}</td>
                        <td>${transaction.id}</td>
                        <td>${transaction.operationType}</td>
                        <td>${transaction.date}</td>
                        <td>${transaction.amount}</td>
                        <td>
                            <form action="transactions/delete" method="post">
                                <input type="hidden" name="id" value=${transaction.accountNumber}/>
                                <button type="submit" class="customer-delete"></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </body>

</html>
