<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.company.model.Account" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <jsp:include page="footer.jsp"/>

        <div class="header">
            Neukonto
        </div>

        <div id="form-container">

            <form action="createAccount/create" method="post" id="customer-form">

            <label for="currency">Währung</label>
            <input class="input-box" type="text" id="currency" name="currency" placeholder="Currency">

             <label for="customerId">Kunde</label>
             <input class="input-box" type="hidden" id="customerId" name="customerId" value="${requestScope.customerId}">

            <button type='submit' class="btn">Erstellen</a>

        </div>

        <script src="/javascript/script.js"></script>

    </body>

</html>