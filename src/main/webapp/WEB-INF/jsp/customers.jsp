<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.company.model.Customer" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <jsp:include page="footer.jsp"/>

        <div class="header">
            Kunden
        </div>

        <div>
            <a href="/createCustomer" class="btn">Erstellen neuer Kunden</a>
        </div>

        <div class="table">

            <table>

                <thead>
                    <tr>
                        <th>Vorname</th>
                        <th>Nachname</th>
                        <th>Geburtstag</th>
                        <th>Adresse</th>
                        <th>Stadt</th>
                        <th>Ausweis</th>
                        <th>Phone</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                <c:forEach items="${requestScope.customers}" var="customer">
                    <tr>
                        <td><a href="/accounts?id=${customer.id}">${customer.firstName}</a></td>
                        <td>${customer.lastName}</td>
                        <td>${customer.birthDay}</td>
                        <td>${customer.address}</td>
                        <td>${customer.city}</td>
                        <td>${customer.passport}</td>
                        <td>${customer.phone}</td>
                        <td>
                            <form action="customers/delete" method="post">
                                <input type="hidden" name="id" value="${customer.id}" />
                                <button type="submit" class="customer-delete"></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </body>

</html>
