<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

     <body class="container">

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>


        <form action='/login' method='post'>
        <input type='text' name='login' placeholder='Login'/>
        <input type='password' name='password' placeholder='Password'/>
        <button type='submit'>Login</button>
        </form>

    </body>

</html>
