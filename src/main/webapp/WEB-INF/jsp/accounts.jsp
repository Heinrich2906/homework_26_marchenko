<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.company.model.Account" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">
         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <%-- Включаем содержимое footer.jsp в текущую позицию на странице --%>
        <jsp:include page="footer.jsp"/>

        <div class="header">
            Konten
        </div>

        <c:if test="${requestScope.customerId != -1}">
            <div>
                <a href="/createAccount?id=${requestScope.customerId}" class="btn">Erstellen ein neues Konto</a>
            </div>
        </c:if>

        <div class="table">

            <table>

                <thead>
                    <tr>
                        <th>Accountnumber</th>
                        <th>Balance</th>
                        <th>Erstellungsdatum</th>
                        <th>Währung</th>
                        <th>Ist blockiert</th>
                        <th>Kundennummer</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                <c:forEach items="${requestScope.accounts}" var="account">
                      <tr>
                        <td><a href="/transactions?id=${account.accountNumber}">${account.accountNumber}</a></td>
                        <td>${account.balance}</td>
                        <td>${account.creationDate}</td>
                        <td>${account.currency}</td>
                        <td>${account.blocked}</td>
                        <td>${account.customerId}</td>
                        <td>
                            <form action="accounts/delete" method="post">
                                <input type="hidden" name="id" value="${account.accountNumber}"/>
                                <button type="submit" class="customer-delete"></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

            </table>

        </div>

    </body>

</html>
